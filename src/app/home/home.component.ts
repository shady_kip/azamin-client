import { Component, OnInit } from '@angular/core';
import {customOwlSettings, customOwlSettings2} from '../settings/owl-carousel/CarouselSettings';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

customOptions = customOwlSettings;
customOptions2 = customOwlSettings2;

  constructor() { }

  ngOnInit() {
  }

}
