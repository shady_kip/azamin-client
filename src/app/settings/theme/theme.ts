/**
 * Created by _ck_ on 3/4/2020.
 */
export interface Theme {
    name: string;
    properties: any;
}

export const light: Theme = {
    name: 'light',
    properties: {
        '--foreground-default': '#08090a',
        '--foreground-primary': '#f4faff',
        '--foreground-secondary': '#f1a9a9',
        '--background-default': '#F4FAFF',
        '--background-primary': '#fcb800',
        '--background-primary-dark': '#dba500',
        '--background-secondary': '#333e48',

        '--error-default': '#EF3E36',
        '--error-dark': '#800600',
        '--error-light': '#FFCECC',

        '--background-tertiary-shadow': '0 1px 3px 0 rgba(92, 125, 153, 0.5)'
    }
};

export const dark: Theme = {
    name: 'dark',
    properties: {
        '--foreground-default': '#f4faff',
        '--foreground-primary': '#08090a',
        '--foreground-secondary': '#f1a9a9',


        '--background-default': '#1c1c1c',
        '--background-secondary': '#000000',

        '--error-default': '#EF3E36',
        '--error-dark': '#800600',
        '--error-light': '#FFCECC',

        '--background-tertiary-shadow': '0 1px 3px 0 rgba(8, 9, 10, 0.5)'
    }
};
