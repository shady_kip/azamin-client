import { OwlOptions } from "ngx-owl-carousel-o";
/**
 * Created by _ck_ on 3/5/2020.
 */

export const customOwlSettings: OwlOptions = {
  loop: true,
  mouseDrag: true,
  touchDrag: true,
  pullDrag: false,
  dots: false,
  navSpeed: 1000,
  autoplay: true,
  navText: ["prev", "next"],
  responsive: {
    0: {
      items: 1
    },
    400: {
      items: 1
    },
    740: {
      items: 1
    },
    940: {
      items: 1
    }
  },
  nav: true
};

export const customOwlSettings2: OwlOptions = {
  loop: true,
  mouseDrag: true,
  touchDrag: true,
  pullDrag: false,
  margin: 5,
  dots: false,
  navSpeed: 1000,
  autoplay: true,
  navText: ["prev", "next"],
  responsive: {
    0: {
      items: 2
    },
    400: {
      items: 3
    },
    740: {
      items: 3
    },
    940: {
      items: 5
    }
  },
  nav: true
};
