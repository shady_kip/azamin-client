import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LostComponent} from './lost/lost.component';
import {ProductDetailsComponent} from './product/product-details/product-details.component';
import {ByCategoriesComponent} from './product/by-categories/by-categories.component';
import {LoginComponent} from './auth/login/login.component';
import {SignupComponent} from './auth/signup/signup.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'product/:slug',
    component: ProductDetailsComponent
  },
  {
    path: 'categories/:category',
    component: ByCategoriesComponent
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: 'auth/signup',
    component: SignupComponent
  },
  {
    path: '**',
    component: LostComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
