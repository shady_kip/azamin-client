import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  showSidebar = false;
  showMegamenu = false;
  sticky = false;
  elementPosition: any;

  @HostListener('window:scroll', []) onWindowScroll() {
    // do some stuff here when the window is scrolled
    const verticalOffset = window.pageYOffset
        || document.documentElement.scrollTop
        || document.body.scrollTop || 0;
    console.log(verticalOffset);
    if (verticalOffset > 150) {
      this.sticky = true;
    } else {
        this.sticky = false;
    }

  }

  constructor() {
  }

    showLeftSideBar = () => {
      this.showSidebar ? this.showSidebar = false : this.showSidebar = true;
    }
    showMegaMenu = () => {
        this.showMegamenu ? this.showMegamenu = false : this.showMegamenu = true;
    }

  ngOnInit() {
  }

}
