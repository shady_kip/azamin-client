import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { FooterComponent } from './footer/footer.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { LostComponent } from './lost/lost.component';
import { AdComponent } from './ad/ad.component';
import {MatButtonModule, MatCardModule, MatDividerModule, MatTabsModule} from '@angular/material';
import {CarouselModule} from 'ngx-owl-carousel-o';
import { ByCategoriesComponent } from './product/by-categories/by-categories.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SignupComponent,
    LoginComponent,
    FooterComponent,
    ProductDetailsComponent,
    HomeComponent,
    LostComponent,
    AdComponent,
    ByCategoriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
      MatButtonModule,
      MatCardModule,
      MatTabsModule,
      CarouselModule,
      MatDividerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
