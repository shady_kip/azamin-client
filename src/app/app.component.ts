import {Component, OnInit, ElementRef, ViewChild, OnDestroy, HostListener} from '@angular/core';
import {ThemeService} from './services/theme.service';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {filter} from 'rxjs/internal/operators';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit, OnDestroy {

constructor(
      private theme: ThemeService,
      private router: Router
  ) {

  }


ngOnInit() {
    this.theme.setLightTheme();
  }
    ngOnDestroy() {

    }

}
